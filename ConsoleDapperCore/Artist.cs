﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleDapperCore
{
    public class Artist
    {
        private List<Track> _tracks = new List<Track>();

        public Artist() // requis par Dapper
        {

        }
        public Artist(int idArtist, string name)
        {
            IdArtist = idArtist;
            Name = name;
        }
        public int IdArtist { get; set; }
        public string Name { get; set; }

        public IEnumerable<Track> Tracks => _tracks;

        // Exercice : Ecrire une méthode AddTrack(Track t) qui ajoute une piste à la liste des pistes de cet artiste
        public void AddTrack(Track t)
        {
            if(t.IdArtist != IdArtist)
            {
                throw new InvalidOperationException("ArtistId inconsistency while adding a track");
            }
            t.Artist = this;
            _tracks.Add(t);
        }

    }
}
