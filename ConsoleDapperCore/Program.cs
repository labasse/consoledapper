﻿using System;
using System.Data.SqlClient;
using System.Linq;

namespace ConsoleDapperCore
{
    class Program
    {
        const string basePath = @"C:\Users\syl\source\ConsoleDapper\ConsoleDapperCore";
        static readonly string connectionStr = $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={basePath}\data\playlists.mdf;Integrated Security=True;Connect Timeout=30";

        static void Main(string[] args)
        {
            ConsoleKeyInfo choix;

            do
            {
                Console.WriteLine("1 - Afficher la liste des artistes");
                Console.WriteLine("2 - Afficher les pistes");
                Console.WriteLine("3 - Trouver un artiste");
                Console.WriteLine("4 - Ajouter un artiste");
                Console.WriteLine("5 - Quitter");
                Console.Write("Votre choix : ");
                choix = Console.ReadKey();

                Console.WriteLine();
                switch(choix.KeyChar)
                {
                    case '1':
                        try
                        {
                            using (var dataContext = new DataContext(connectionStr))
                            {
                                foreach (var artist in dataContext.AllArtists)
                                {
                                    Console.WriteLine($"{artist.Name} ({artist.IdArtist})");
                                    foreach (var track in artist.Tracks)
                                    {
                                        Console.WriteLine($"- {track.Title}");
                                    }
                                }
                            }
                        }
                        catch (SqlException e)
                        {
                            Console.Error.WriteLine($"Database connection: {e.Message}");
                        }
                        break;
                    case '2':
                        try
                        {
                            using (var dataContext = new DataContext(connectionStr))
                            {
                                Console.WriteLine("+-------------------------------------------------+-------------+");
                                Console.WriteLine("|                   PISTE                         |    DUREE    |");
                                Console.WriteLine("+-------------------------------------------------+-------------+");
                                foreach (var track in dataContext.AllTracks)
                                {
                                    Console.WriteLine($"| {track.Title,-47} | {track.DurationAsTimeSpan,11:mm':'ss} |");
                                }
                                Console.WriteLine("+-------------------------------------------------+-------------+");
                                Console.WriteLine($"                                         Total    | {dataContext.DurationTotal,11:mm':'ss} |");
                                Console.WriteLine("                                                  +-------------+");
                            }
                        }
                        catch (SqlException e)
                        {
                            Console.Error.WriteLine($"Database connection: {e.Message}");
                        }
                        break;
                    case '3':
                        {
                            Console.Write("Votre recherche : ");
                            var search = Console.ReadLine();

                            using(var dataContext=new DataContext(connectionStr))
                            {
                                var result = dataContext.FindArtistByName(search);
                                
                                foreach(var artist in result)
                                {
                                    Console.WriteLine($"- {artist.Name}");
                                }
                            }
                            break;
                        }
                    case '4':
                        using (var dataContext = new DataContext(connectionStr))
                        {
                            // Etape 1 : Demander à l'utilisateur le nom du nouvel artiste (name)
                            Console.Write("Nom du nouvel artiste : ");
                            var name = Console.ReadLine();

                            // Etape 2 : Vérifier si il n'existe pas un artiste qui commence par ce nom (FindArtistByName)
                            var result = dataContext.FindArtistByName(name);

                            if(result.Count()>0)
                            {
                                Console.Error.WriteLine($"Il y a déjà {result.Count()} artiste-s avec ce nom.");
                            }
                            else
                            {
                                // Etape 3 : Ajouter l'artiste avec le code suivant : (Save à créer)
                                var newArtist = new Artist();

                                newArtist.Name = name;
                                dataContext.Save(newArtist);
                                Console.WriteLine($"L'artiste a été créé avec l'identifiant {newArtist.IdArtist}");
                            }
                        }
                        break;
                    case '5':
                        break;
                    default:
                        Console.Error.WriteLine("Touche non gérée");
                        break;
                }
            }
            while (choix.KeyChar != '5');
        }
    }
}
