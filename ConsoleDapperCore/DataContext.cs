﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;

namespace ConsoleDapperCore
{
    public class DataContext : IDisposable
    {
        private Dictionary<int, Artist> _allArtists = new Dictionary<int, Artist>();
        
        private SqlConnection _db;

        public DataContext(string connectionString)
        {
            _db = new SqlConnection(connectionString);

            _db.Query<Artist, Track, Artist>(@"
                SELECT *, Track.IdTrack, Track.Title, Track.Path, Track.Duration
                FROM Artist INNER JOIN Track ON Artist.IdArtist = Track.IdArtist 
                ORDER BY Name",
                (artist, track) =>
                {
                    // Si l'artiste existe déjà
                    if(_allArtists.ContainsKey(artist.IdArtist))
                    {
                        artist = _allArtists[artist.IdArtist];
                    }
                    // Si l'artiste n'existe pas déjà
                    else
                    {
                        _allArtists.Add(artist.IdArtist, artist);
                    }
                    artist.AddTrack(track);
                    return artist;
                },
                splitOn:"IdArtist"
             );

        }
        private TimeSpan _total;
        public TimeSpan ComputedDuration => _total; // Risqué car ne tient pas compte des modifications ultérieures des pistes

        public IEnumerable<Artist> AllArtists => _allArtists.Values;
        
        public IEnumerable<Track> AllTracks => _db.Query<Track>("SELECT [IdTrack], [Title], [Path], [Duration], [IdArtist] FROM Track"); // AFAIRE après avoir adapté la classe Track pour Dapper

        public TimeSpan DurationTotal => TimeSpan.FromSeconds((int)_db.ExecuteScalar("SELECT SUM(Duration) FROM Track"));

        public IEnumerable<Artist> FindArtistByName(string search)
        {
            return _db.Query<Artist>("SELECT * FROM Artist WHERE Name LIKE @Search", new { Search = search+'%' });
        }

        public void Save(Artist artist)
        {
            if(artist.IdArtist == 0)
            {
                // Inserer
                var sql = "INSERT INTO Artist(Name) values(@Name); SELECT CAST(SCOPE_IDENTITY() as int)";
                var returnId = _db.ExecuteScalar<int>(sql, artist);

                artist.IdArtist = returnId;
                _allArtists.Add(artist.IdArtist, artist);
            }
            else
            {
                // Mise à jour
                _db.Execute("UPDATE Artist SET Name=@Name WHERE IdArtist=@IdArtist", artist); 
            }
        }

        void RemoveArtist(Artist artist)
        {
            _db.Execute("DELETE FROM Artist WHERE IdArtist=@IdArtist", artist);
            _allArtists.Remove(artist.IdArtist);
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    } 
}
