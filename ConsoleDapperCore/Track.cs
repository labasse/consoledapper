﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleDapperCore
{
    public class Track
    {
        private string _title, _path;

        public Track()
        {

        }
        public Track(int idTrack, string title, int duration, string path)
        {
            IdTrack = idTrack;
            Title = title;
            Duration = duration;
            Path = path;
        }
        public int IdTrack { get; set; }
        
        public string Title {
            get => _title;
            set
            {
                _title = value.Trim();
            }
        }
        public int Duration { get; set; }
        public string Path
        {
            get => _path;
            set
            {
                _path = value.Trim();
            }
        }
        public int IdArtist { get; set; }
        public Artist Artist { get; set; }

        public TimeSpan DurationAsTimeSpan => TimeSpan.FromSeconds(Duration);

        // public TimeSpan DurationAsTimeSpan_v2
        // {
        //     get
        //     {
        //         return TimeSpan.FromSeconds(Duration);
        //     }
        // }
    }
}
